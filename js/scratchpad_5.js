// // // // const names = ['John', 'Jack', 'Jane', 'James', 'Jogesh', 'Janardhan', 'Josh'];

// // // // // for (let i = 0; i < names.length; i++) {
// // // // // 	console.log(names[i]);
// // // // // }

// // // // for (let name of names) {
// // // // 	console.log(name);
// // // // }

// // // // let fullName = 'John Smith';

// // // // for (let char of fullName) {
// // // // 	console.log(char);
// // // // }

// // // const matrix = [
// // // 	[1, 4, 7],
// // // 	[9, 7, 2],
// // // 	[9, 4, 6],
// // // ];

// // // for (let row of matrix) {
// // // 	for (let col of row) {
// // // 		console.log(col);
// // // 	}
// // // }

// // // const cats = ['fashion', 'mobiles', 'books'];
// // // const prods = ['tshirt', 'samsung', '1984'];

// // // for (let i = 0; i < cats.length; i++) {
// // // 	console.log(cats[i], prods[i]);
// // // }

// // const productPrices = {
// // 	Apple: 80000,
// // 	OnePlus: 50000,
// // 	Samsung: 90000,
// // };

// // for (let key of Object.keys(productPrices)) {
// // 	console.log(key, productPrices[key]);
// // }

// // for (let key in productPrices) {
// // 	console.log(key, productPrices[key]);
// // }

// // // for (let item of Object.keys(productPrices)) {
// // // 	console.log(item);
// // // }

// // // for (let item of Object.values(productPrices)) {
// // // 	console.log(item);
// // // }

// // function greet() {
// // 	console.log('Hello World');
// // 	console.log('Hello Universe');
// // }

// // greet();

// function flipCoin() {
// 	const randomNum = Math.random();
// 	if (randomNum > 0.5) {
// 		console.log('HEADS');
// 	} else {
// 		console.log('TAILS');
// 	}
// }

// flipCoin();
// flipCoin();
// flipCoin();

// function rollDie() {
// 	let roll = Math.floor(Math.random() * 6) + 1;
// 	console.log(`Rolled: ${roll}`);
// }

// function throwDice() {
// 	rollDie();
// 	rollDie();
// 	rollDie();
// }

// throwDice();

// function greet() {
// 	console.log('Hello World');
// }

// greet();
// greet();
// greet();
// greet();
// greet();

// function greet(fullName) {
// 	console.log(`Hello, ${fullName}`);
// }

// greet('John Doe');

// function greet(firstName, lastName) {
// 	console.log(arguments);
// 	console.log(`Hello, ${firstName} ${lastName}`);
// }

// function greet() {
// 	console.log(`Hello, ${arguments[0]} ${arguments[1]}`);
// }

// greet('Jane', 30);

// function rollDie() {
// 	let roll = Math.floor(Math.random() * 6) + 1;
// 	console.log(`Rolled: ${roll}`);
// }

// function throwDice(times) {
// 	for (let i = 0; i < times; i++) {
// 		rollDie();
// 	}
// }

// throwDice(10);

// function greet(firstName, lastName) {
// 	return `Hello, ${firstName} ${lastName}`;
// }

// const result = greet('Jane', 'Smith');
// console.log(result);

// function isNumber(value) {
// 	if (typeof value !== 'number') {
// 		return false;
// 	}

// 	return true;
// }

// console.log(isNumber(200));

// let fullName = 'John Doe';

// function greet() {
// 	let fullName = 'Jane Smith';
// 	console.log(fullName);
// }

// greet();
// console.log(fullName);

// let fullName = 'John Doe';

// if (true) {
// 	var fullName = 'Jane Smith';
// 	console.log(fullName);
// }

// console.log(fullName);

// for (var i = 0; i < 10; i++) {
// 	console.log(i);
// }

// console.log('OUTSIDE THE LOOP', i);

// var i = 10;
// var i = 'hello';

// let fullName = 'John Doe';

// function outer() {
// 	// let fullName = 'John Doe';

// 	function inner() {
// 		let fullName = 'Jane Smith';
// 		console.log(fullName);
// 	}

// 	inner();
// 	console.log(fullName);
// }

// outer();

// function greet() {
// 	return 'Hello World';
// }

// // let a = 10;
// let a = greet;

// console.log(a());

// const hello = function () {
// 	console.log('Hello World');
// };

// console.log(hello);

// const math = [
// 	10,
// 	20,
// 	30,
// 	function () {
// 		return 'Hello World';
// 	},
// 	40,
// 	50,
// 	60,
// 	'test',
// ];

// console.log(math[3]());

// const math = {
// 	PI: 3.14,
// 	add: function (a, b) {
// 		return a + b;
// 	},
// 	sub: function (a, b) {
// 		return a - b;
// 	},
// 	mul: function (a, b) {
// 		return a * b;
// 	},
// 	div: function (a, b) {
// 		return a / b;
// 	},
// };

// console.log(math.add(10, 20));

// console.log(console);

// console.table({ firstName: 'John', lastName: 'Doe', age: 20 });

(function () {
	console.log('hello World');
})();
