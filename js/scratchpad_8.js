// // // console.log(document.all);

// // // const listItem = document.getElementById('specialItem');
// // // console.dir(listItem);

// // // const lis = document.getElementsByTagName('li');
// // // console.log(lis);

// // // const importantEls = document.getElementsByClassName('imp');
// // // console.log(importantEls);

// // // const ul = document.getElementsByTagName('ul')[0];
// // // const li = ul.getElementsByClassName('imp');
// // // console.log(li);

// // // const h1 = document.getElementsByTagName('h1')[0];
// // // const h1 = document.querySelectorAll('li');
// // // console.log(h1);

// // // const li = document.querySelector('#specialItem');
// // // console.log(li);

// // // const li = document.querySelector('.imp');
// // // console.log(li);

// // // const h1 = document.querySelector('h1');
// // // // console.log(h1.innerText);
// // // // h1.innerText = new Date().toLocaleDateString();

// // // setInterval(() => {
// // // 	h1.innerText = Math.random();
// // // }, 500);

// // // const p = document.querySelector('p');
// // // console.log(p.textContent);

// // // const h1 = document.querySelector('h1');
// // // h1.innerHTML = 'Hello this is <u>something</u>';
// // // h1.innerHTML += ' - <strike>This is cancelled</strike>';

// // const a = document.querySelector('a');
// // // console.log(a.href);
// // // a.href = 'https://bing.com';
// // // a.id = 'anchor';
// // // a.className = 'imp-link';

// // // console.log(a.getAttribute('href'));
// // // console.log(a.href);

// // // console.log(a.getAttribute('id'));
// // // console.log(a.id);

// // // a.setAttribute('href', 'https://bing.com');

// // // const li = document.querySelector('li.imp');
// // // // console.log(li.parentElement.parentElement.parentElement);
// // // console.log(li.children);

// // // const lis = document.querySelectorAll('li');

// // // for (let li of lis) {
// // // 	setInterval(() => {
// // // 		li.innerText = Math.random();
// // // 	}, 100);
// // // }

// // // const h1 = document.querySelector('h1');
// // // h1.style.color = 'red';
// // // h1.style.backgroundColor = 'yellow';

// // // const colors = [
// // // 	'red',
// // // 	'yellow',
// // // 	'blue',
// // // 	'green',
// // // 	'orange',
// // // 	'purple',
// // // 	'pink',
// // // 	'gold',
// // // 	'silver',
// // // 	'gray',
// // // 	'maroon',
// // // 	'aqua',
// // // ];

// // // const lis = document.querySelectorAll('li');
// // // document.body.style.transition = 'all 0.5s';

// // // for (let li of lis) {
// // // 	setInterval(() => {
// // // 		const randomNum1 = Math.floor(Math.random() * colors.length);
// // // 		const randomNum2 = Math.floor(Math.random() * colors.length);
// // // 		const randomNum3 = Math.floor(Math.random() * colors.length);
// // // 		li.style.transition = 'all 0.2s';
// // // 		li.style.color = colors[randomNum1];
// // // 		li.style.fontSize = `${Math.floor(Math.random() * 100) + 1}px`;
// // // 		li.style.backgroundColor = colors[randomNum2];
// // // 		document.body.style.backgroundColor = colors[randomNum3];
// // // 	}, 200);
// // // }

// // const li = document.querySelector('li');
// // // li.className = 'todo done';

// // // console.log(li.classList);

// // li.classList.add('done');
// // li.classList.remove('hello');
// // li.classList.toggle('world');

// // console.log('Hello world');

// // const root = document.querySelector('#root');

// // const h3 = document.createElement('h3'); // <h3></h3>
// // h3.innerText = 'This is an h3 element.'; // <h3>This is an h3 element.</h3>
// // h3.style.color = 'red'; // <h3 style="color: red;">This is an h3 element.</h3>

// // root.appendChild(h3);

// // const ul = document.querySelector('ul');
// // const currentLi = ul.querySelectorAll('li')[1];

// // // console.log(currentLi);
// // const newLi = document.createElement('li');
// // newLi.innerText = 'This is something new';
// // newLi.classList.add('todo');

// // // ul.appendChild(newLi);
// // ul.insertBefore(newLi, currentLi);

// const p = document.querySelector('p');
// const b = document.createElement('b');
// const i = document.createElement('i');
// b.innerText = 'BOLD TEXT';
// i.innerText = 'ITALIC TEXT';

// // p.insertAdjacentElement('afterend', b);

// p.append(b, i);
// p.prepend(b, i);

const p = document.querySelector('p');
p.remove();
