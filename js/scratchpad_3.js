// // let age = 13;

// // if (age >= 18) {
// // 	console.log('Hello, you can come in.');
// // }

// // let num = 22;

// // if (num % 2 == 0) {
// // 	console.log('EVEN NUMBER');
// // }

// // let age = 90;

// // if (age >= 65) {
// // 	console.log('Drinks are free.');
// // } else if (age >= 21) {
// // 	console.log('You can enter and you can drink.');
// // } else if (age >= 18) {
// // 	console.log('You can enter but you cannot drink.');
// // } else {
// // 	console.log('Sorry, you are not allowed to enter.');
// // }

// let age = 2000;

// if (age >= 18) {
// 	console.log('You can enter but you cannot drink.');
// } else if (age >= 21) {
// 	console.log('You can enter and you can drink.');
// } else if (age >= 65) {
// 	console.log('Drinks are free.');
// } else {
// 	console.log('Sorry, you are not allowed to enter.');
// }

// let password = 'hell';

// if (password.length >= 8) {
// 	if (password.indexOf(' ') !== -1) {
// 		console.log('Password cannot contain spaces');
// 	} else {
// 		console.log('Valid password');
// 	}
// } else {
// 	console.log('Invalid password');
// }

// if ('') {
// 	console.log('Hello world');
// }

// let loggedInUser = null;

// if (loggedInUser) {
// 	console.log('Welcome to the site');
// } else {
// 	console.log('Please login to continue');
// }

// let age = 2;

// if (age >= 18 && age < 21) {
// 	console.log('You can enter but you cannot drink.');
// } else if (age >= 21 && age < 65) {
// 	console.log('You can enter and you can drink.');
// } else if (age >= 65) {
// 	console.log('Drinks are free.');
// } else {
// 	console.log('Sorry, you are not allowed to enter.');
// }

// let day = 1;

// switch (day) {
// 	case 1:
// 		console.log('Mon');
// 		break;
// 	case 2:
// 		console.log('Tue');
// 		break;
// 	case 3:
// 		console.log('Wed');
// 		break;
// 	case 4:
// 		console.log('Thu');
// 		break;
// 	case 5:
// 		console.log('Fri');
// 		break;
// 	case 6:
// 		console.log('Sat');
// 		break;
// 	case 7:
// 		console.log('Sun');
// 		break;
// 	default:
// 		console.log('Invalid day code.');
// }

let currentStatus = 'online';

let color =
	currentStatus === 'online'
		? 'green'
		: currentStatus === 'offline'
		? 'red'
		: 'yellow';

// let color;

// if (currentStatus === 'online') {
// 	color = 'green';
// } else if (currentStatus === 'offline') {
// 	color = 'red';
// } else {
// 	color = 'yellow';
// }

console.log(color);
