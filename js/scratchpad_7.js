// // // // // // // // // // const nums = [2, 3, 4, 7, 6, 8, 13, 10, 19, 12, 14, 22, 21, 16];

// // // // // // // // // // const doubles = nums.map(num => num * 2);

// // // // // // // // // // console.log(doubles);

// // // // // // // // // // // const square1 = function (num) {
// // // // // // // // // // // 	return num ** 2;
// // // // // // // // // // // }

// // // // // // // // // // // const square2 = n => n ** 2;

// // // // // // // // // // // console.log(square1(8));
// // // // // // // // // // // console.log(square2(8));

// // // // // // // // // const movies = ['The Terminator', 'The Avengers', 'Jurassic Park', 'Titanic'];

// // // // // // // // // let result = movies.find((movie) => movie[0] === 'X');
// // // // // // // // // console.log(result);

// // // // // // // // const books = [
// // // // // // // // 	{
// // // // // // // // 		title: 'The Shining',
// // // // // // // // 		author: 'Stephen King',
// // // // // // // // 		rating: 4.1,
// // // // // // // // 	},
// // // // // // // // 	{
// // // // // // // // 		title: 'Sacred Games',
// // // // // // // // 		author: 'Vikram Chandra',
// // // // // // // // 		rating: 4.5,
// // // // // // // // 	},
// // // // // // // // 	{
// // // // // // // // 		title: '1984',
// // // // // // // // 		author: 'George Orwell',
// // // // // // // // 		rating: 4.9,
// // // // // // // // 	},
// // // // // // // // 	{
// // // // // // // // 		title: 'The Alchemist',
// // // // // // // // 		author: 'Paulo Coelho',
// // // // // // // // 		rating: 3.5,
// // // // // // // // 	},
// // // // // // // // 	{
// // // // // // // // 		title: 'The Great Gatsby',
// // // // // // // // 		author: 'F. Scott Fitzgerald',
// // // // // // // // 		rating: 3.8,
// // // // // // // // 	},
// // // // // // // // ];

// // // // // // // // const result = books.filter((book) => book.rating < 4);
// // // // // // // // console.log(result);

// // // // // // // const names = ['jack', 'james', 'john', 'jane', 'josh', 'jrad'];

// // // // // // // // const result = names.every((name) => name[0] === 'j');
// // // // // // // // console.log(result);

// // // // // // // const result = names.some((name) => name[0] !== 'j');
// // // // // // // console.log(result);

// // // // // // // const prices = [500.4, 211, 23, 5, 4, 22.2, -23.2, 9233];

// // // // // // // const result = prices.sort((a, b) => b - a);

// // // // // // // console.log(result);

// // // // // // // const books = [
// // // // // // // 	{
// // // // // // // 		title: 'The Shining',
// // // // // // // 		author: 'Stephen King',
// // // // // // // 		rating: 4.1,
// // // // // // // 	},
// // // // // // // 	{
// // // // // // // 		title: 'Sacred Games',
// // // // // // // 		author: 'Vikram Chandra',
// // // // // // // 		rating: 4.5,
// // // // // // // 	},
// // // // // // // 	{
// // // // // // // 		title: 'Half Girlfriend',
// // // // // // // 		author: 'Chetan Bhagat',
// // // // // // // 		rating: 0.2,
// // // // // // // 	},
// // // // // // // 	{
// // // // // // // 		title: '1984',
// // // // // // // 		author: 'George Orwell',
// // // // // // // 		rating: 4.9,
// // // // // // // 	},
// // // // // // // 	{
// // // // // // // 		title: 'The Alchemist',
// // // // // // // 		author: 'Paulo Coelho',
// // // // // // // 		rating: 3.5,
// // // // // // // 	},
// // // // // // // 	{
// // // // // // // 		title: 'The Great Gatsby',
// // // // // // // 		author: 'F. Scott Fitzgerald',
// // // // // // // 		rating: 3.8,
// // // // // // // 	},
// // // // // // // ];

// // // // // // // books.sort((a, b) => b.rating - a.rating);
// // // // // // // console.log(books);

// // // // // // // const nums = [1, 2, 3, 4, 5];

// // // // // // // const result = nums.reduce((acc, currVal) => {
// // // // // // // 	return acc * currVal;
// // // // // // // });

// // // // // // // console.log(result);

// // // // // // // acc    currVal
// // // // // // // 1      2
// // // // // // // 3      3
// // // // // // // 6      4
// // // // // // // 10     5
// // // // // // // 15

// // // // // // const nums = [123, 34, 45, 65, 76, 6, 678, 45, 3];

// // // // // // const result = nums.reduce((acc, currVal) => {
// // // // // // 	if (currVal > acc) {
// // // // // // 		return currVal;
// // // // // // 	}
// // // // // // 	return acc;
// // // // // // });

// // // // // // console.log(result);

// // // // // // // acc    currVal
// // // // // // // 123		34
// // // // // // // 123		45
// // // // // // // 123		65
// // // // // // // 123		678
// // // // // // // 678		45

// // // // // const nums = [2, 3, 4, 7, 6, 8, 13, 10, 19, 12, 14, 22, 21, 16];

// // // // // console.log(
// // // // // 	`The sum of all the numbers in nums is ${nums.reduce(
// // // // // 		(acc, currVal) => acc + currVal
// // // // // 	)}.`
// // // // // );

// // // // // // let total = 0;
// // // // // // for (let num of nums) {
// // // // // // 	total += num;
// // // // // // }
// // // // // // console.log(`The sum of all the numbers in nums is ${total}.`);

// // // // // greet();

// // // // // const greet = function () {
// // // // // 	console.log('hello world!!!');
// // // // // };

// // // // // const nums = [1, 2, 3, 4, 5];

// // // // // const result = nums.reduce((acc, currVal) => acc * currVal, {});

// // // // // console.log(result);

// // // // // function raiseTo(a = 1, b = 1) {
// // // // // 	return a ** b;
// // // // // }

// // // // // console.log(raiseTo(10));

// // // // // const nums = [3423, 234, 434, 5, 4567, 678, 7, 23312, 3, 5, 6];

// // // // // const result = Math.max(...nums);

// // // // // console.log(result);

// // // // function dummy(a, b, c) {
// // // // 	console.log(a);
// // // // 	console.log(b);
// // // // 	console.log(c);
// // // // }

// // // // const names = ['John', 'jack', 'Jane'];

// // // // dummy(...'hello');

// // // // function add(a, b, c = 0) {
// // // // 	return a + b + c;
// // // // }

// // // // console.log(add(10, 20));

// // // // function add(a, b, ...nums) {
// // // // 	console.log(a);
// // // // 	console.log(b);
// // // // 	console.log(nums);
// // // // }

// // // // add(10, 20, 30, 40, 50, 60, 70, 80, 90, 100);

// // // // function add(...nums) {
// // // // 	// return nums.reduce((acc, currVal) => acc + currVal);

// // // // 	let total = 0;
// // // // 	for (let num of nums) {
// // // // 		total += num;
// // // // 	}
// // // // 	return total;
// // // // }

// // // // console.log(add(10, 20, 30, 40, 50, 60, 70, 80, 90, 100));

// // // // const users = ['john', 'jack', 'jane'];

// // // // // const admin = users[0];
// // // // // const mod = users[1];
// // // // // const cust = users[2];

// // // // // const [admin, mod, cust] = users;
// // // // // const [admin, , cust] = users;
// // // // const [admin, ...others] = users;

// // // // console.log(admin, others);

// // // // const user = {
// // // // 	firstName: 'John',
// // // // 	lastName: 'Doe',
// // // // 	email: 'john.doe@gmail.com',
// // // // 	phone: 99982234567,
// // // // };

// // // // const { firstName, lastName, ...others } = user;

// // // // console.log(firstName, lastName, others);

// // // function profile({ firstName, lastName, age, jobTitle, company }) {
// // // 	console.log(
// // // 		`Hello my name is ${firstName} ${lastName} and I am ${age} years old. I work as a ${jobTitle} at ${company}.`
// // // 	);
// // // }

// // // profile({
// // // 	firstName: 'John',
// // // 	lastName: 'Doe',
// // // 	age: 25,
// // // 	jobTitle: 'Programmer',
// // // 	company: 'Chaggan Infotech',
// // // });

// // const movieReviews = [4.5, 5.0, 3.2, 2.1, 4.7, 3.8, 3.1, 3.9, 4.4];
// // const highest = Math.max(...movieReviews);
// // const lowest = Math.min(...movieReviews);

// // let total = 0;
// // movieReviews.forEach((rating) => (total += rating));
// // const average = total / movieReviews.length;

// // const info = {
// // 	highest,
// // 	lowest,
// // 	average,
// // 	PI: 3.1415,
// // };

// // console.log(info);

// // const username = 'janedoe';
// // const role = 'admin';

// // const user = { [role]: username };
// // console.log(user);

// // const addProperty = (obj, k, v) => {
// // 	return { ...obj, [k]: v };
// // };

// // console.log(addProperty({ firstName: 'John' }, 'lastName', 'Doe'));

// const math = {
// 	multiply(x, y) {
// 		return x * y;
// 	},
// 	divide(x, y) {
// 		return x / y;
// 	},
// 	subtract(x, y) {
// 		return x - y;
// 	},
// };

// console.log(math.subtract(10, 20));

// function greet() {
// 	console.log(
// 		`Hello my name is ${this.firstName} ${this.lastName} and I am ${this.age} years old.`
// 	);
// }

// // window.greet();

// const user = {
// 	firstName: 'John',
// 	lastName: 'Doe',
// 	age: 20,
// 	greet: function () {
// 		console.log(
// 			`Hello my name is ${this.firstName} ${this.lastName} and I am ${this.age} years old.`
// 		);
// 	},
// };

// greet();
// user.greet();

// console.log(this);

// function greet() {
// 	console.log(
// 		`Hello my name is ${this.firstName} ${this.lastName} and I am ${this.age} years old.`
// 	);
// }

// const user1 = {
// 	firstName: 'John',
// 	lastName: 'Doe',
// 	age: 20,
// 	greet,
// };

// const user2 = {
// 	firstName: 'Jane',
// 	lastName: 'Smith',
// 	age: 30,
// 	greet: function () {
// 		console.log(
// 			`Hello my name is ${this.firstName} ${this.lastName} and I am ${this.age} years old.`
// 		);
// 	},
// };

// const greet = user2.greet;
// greet();

// user1.greet();
// user2.greet();
// const user = {
// 	firstName: 'Jane',
// 	lastName: 'Smith',
// 	age: 30,
// 	greet: () => {
// 		// this = user
// 		console.log(
// 			`Hello my name is ${this.firstName} ${this.lastName} and I am ${this.age} years old.`
// 		);
// 	},
// };

// user.greet();

const hellos = {
	messages: [
		'hello world',
		'hello universe',
		'hello darkness',
		'hello hello',
		'heylo',
	],
	pickMsg: function () {
		const index = Math.floor(Math.random() * this.messages.length);
		return this.messages[index];
	},
	start: function () {
		setInterval(() => {
			// this = hellos
			console.log(this.pickMsg());
		}, 1000);
	},
};

// console.log(hellos.pickMsg());

hellos.start();
