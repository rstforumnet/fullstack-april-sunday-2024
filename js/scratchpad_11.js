// // function initializeDeck() {
// // 	const deck = [];
// // 	const suits = ['hearts', 'diamonds', 'spades', 'clubs'];
// // 	const values = '2,3,4,5,6,7,8,9,10,J,Q,K,A';
// // 	for (let value of values.split(',')) {
// // 		for (let suit of suits) {
// // 			deck.push({ value, suit });
// // 		}
// // 	}
// // 	return deck;
// // }

// // function drawCard(deck, drawnCards) {
// // 	const card = deck.pop();
// // 	drawnCards.push(card);
// // 	return card;
// // }

// // function drawMultiple(numCards, deck, drawnCards) {
// // 	const cards = [];
// // 	for (let i = 0; i < numCards; i++) {
// // 		cards.push(drawCard(deck, drawnCards));
// // 	}
// // 	return cards;
// // }

// // function shuffle(deck) {
// // 	// loop over array backwards
// // 	for (let i = deck.length - 1; i > 0; i--) {
// // 		// pick random index before current element
// // 		let j = Math.floor(Math.random() * (i + 1));
// // 		[deck[i], deck[j]] = [deck[j], deck[i]];
// // 	}
// // 	return deck;
// // }

// // const deck1 = initializeDeck();
// // const hand = [];
// // shuffle(deck1);

// // drawCard(deck1, hand);
// // drawCard(deck1, hand);
// // drawCard(deck1, hand);
// // drawCard(deck1, hand);
// // drawMultiple(3, deck1, hand);
// // drawMultiple(3, deck1, hand);
// // console.log(deck1);
// // console.log(hand);

// function makeDeck() {
// 	return {
// 		deck: [],
// 		drawnCards: [],
// 		suits: ['hearts', 'diamonds', 'spades', 'clubs'],
// 		values: '2,3,4,5,6,7,8,9,10,J,Q,K,A',
// 		initializeDeck() {
// 			for (let value of this.values.split(',')) {
// 				for (let suit of this.suits) {
// 					this.deck.push({ value, suit });
// 				}
// 			}
// 			return this.deck;
// 		},
// 		drawCard() {
// 			const card = this.deck.pop();
// 			this.drawnCards.push(card);
// 			return card;
// 		},
// 		drawMultiple(numCards) {
// 			const cards = [];
// 			for (let i = 0; i < numCards; i++) {
// 				cards.push(this.drawCard());
// 			}
// 			return cards;
// 		},
// 		shuffle() {
// 			const { deck } = this;
// 			for (let i = deck.length - 1; i > 0; i--) {
// 				let j = Math.floor(Math.random() * (i + 1));
// 				[deck[i], deck[j]] = [deck[j], deck[i]];
// 			}
// 		},
// 	};
// }

// const deck1 = makeDeck();
// const deck2 = makeDeck();

// console.log(deck1);
// console.log(deck2);

// // deck1.initializeDeck();
// // deck1.shuffle();
// // deck1.drawCard();
// // deck1.drawCard();
// // deck1.drawCard();
// // deck1.drawCard();
// // console.log(deck1);

function person(firstName, lastName, age) {
	return {
		firstName,
		lastName,
		age,
		greet() {
			return `Hello, my name is ${firstName} ${lastName}`;
		},
	};
}

class Person {
	constructor(firstName, lastName, age) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
	}

	greet() {
		return `Hello, my name is ${firstName} ${lastName}`;
	}
}

// function Person(firstName, lastName, age) {
// 	this.firstName = firstName;
// 	this.lastName = lastName;
// 	this.age = age;
// }

// Person.prototype.greet = function () {
// 	return `Hello, my name is ${firstName} ${lastName}`;
// };

const john = person('John', 'Doe', 20);
const jane = person('Jane', 'Smith', 25);
const jack = {
	firstName: 'Jack',
	lastName: 'Roe',
	age: 30,
	greet() {
		return `Hello, my name is ${firstName} ${lastName}`;
	},
};
const jill = new Person('Jill', 'Moe', 26);
const jay = new Person('Jay', 'Y', 50);

console.log(john);
console.log(jane);
console.log(jack);
console.log(jill);
console.log(jay);

// console.log(john.greet());
// console.log(jane.greet());
