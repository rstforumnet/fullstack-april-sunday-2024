// // // // // function add(a, b) {
// // // // // 	return a + b;
// // // // // }

// // // // // const test = 10;
// // // // // const hello = add;

// // // // // console.log(hello(10, 20));

// // // // function math(a, b, fn) {
// // // // 	return fn(a, b);
// // // // }

// // // // function add(a, b) {
// // // // 	return a + b;
// // // // }

// // // // console.log(math(10, 20, add));

// // // // // function add(a, b) {
// // // // // 	return a + b;
// // // // // }

// // // // // console.log(add(10, 20));
// // // // // console.log(add(10));
// // // // // console.log(add());
// // // // // console.log(add(10, 20, 30, 40));
// // // // // console.log(add('Hello', 'World'));

// // // // function multipleGreets(fn) {
// // // // 	fn();
// // // // 	fn();
// // // // 	fn();
// // // // }

// // // // function sayHello() {
// // // // 	console.log('Hello World!');
// // // // }
// // // // function sayGoodbye() {
// // // // 	console.log('Bye World!');
// // // // }

// // // // multipleGreets(sayGoodbye);

// // // // function raiseBy(num) {
// // // // 	return function (x) {
// // // // 		return x ** num;
// // // // 	};
// // // // }

// // // // const square = raiseBy(2);
// // // // const cube = raiseBy(3);
// // // // const hypercube = raiseBy(4);

// // // // console.log(square(5));
// // // // console.log(cube(5));
// // // // console.log(hypercube(5));

// // // // function math(a, b, fn) {
// // // // 	return fn(a, b);
// // // // }

// // // // function add(a, b) {
// // // // 	return a + b;
// // // // }

// // // // console.log(
// // // // 	math(10, 20, function (a, b) {
// // // // 		return a * b;
// // // // 	})
// // // // );

// // // // setTimeout(function () {
// // // // 	console.log('Hello World');
// // // // }, 5000);

// // // setInterval(function () {
// // // 	console.log('Hello World');
// // // }, 1000);

// // // greet();

// // // function greet() {
// // // 	console.log('Hello World');
// // // }

// // console.log(hello);
// // const hello = 'world';

// // const nums = [9, 2, 4, 6, 2, 3, 7, 6];

// // // for (let num of nums) {
// // // 	console.log(num);
// // // }

// // nums.forEach(function (num) {
// // 	console.log(num, 'Hello World');
// // });

// const movies = [
// 	{
// 		title: 'Avengers',
// 		rating: 1.1,
// 	},
// 	{
// 		title: 'Dr. Strange',
// 		rating: 1.9,
// 	},
// 	{
// 		title: 'Tenet',
// 		rating: 4.3,
// 	},
// 	{
// 		title: 'Joker',
// 		rating: 4.7,
// 	},
// ];

// movies.forEach(function (movie, i) {
// 	console.log(`${i}: ${movie.title} has a rating of ${movie.rating}`);
// });

// const names = ['john', 'jack', 'jane', 'james'];
// const nums = [1, 2, 3, 8, 5, 6, 34, 2, 3];

// const uppercaseNames = names.map(function (name) {
// 	return name.toUpperCase();
// });

// const doubles = nums.map(function (num) {
// 	return { doubled: num * 2, isEven: num % 2 === 0 };
// });

// const uppercaseNames = [];
// const doubles = [];

// for (let name of names) {
// 	uppercaseNames.push(name.toUpperCase());
// }

// for (let num of nums) {
// 	doubles.push(num * 2);
// }

// console.log(uppercaseNames);
// console.log(doubles);

const names = ['john', 'jack', 'jane', 'james'];

const upperNames = names.map(function (name) {
	return { upperName: name.toUpperCase(), randomNum: Math.random() };
});

console.log(upperNames); // ["hello world", "hello world"]
