// // // // // const nums = [
// // // // // 	[1, 2, 3],
// // // // // 	[1, 2, 3],
// // // // // 	[1, 2, 3],
// // // // // ];

// // // // // const product = [
// // // // // 	'iPhone 15',
// // // // // 	'Apple',
// // // // // 	'Some description....',
// // // // // 	100000,
// // // // // 	500,
// // // // // 	true,
// // // // // 	'https://url....',
// // // // // ];

// // // // // const products = [
// // // // // 	[
// // // // // 		'iPhone 15',
// // // // // 		'Apple',
// // // // // 		'Some description....',
// // // // // 		500,
// // // // // 		100000,
// // // // // 		true,
// // // // // 		'https://url....',
// // // // // 	],
// // // // // 	[
// // // // // 		'iPhone 15',
// // // // // 		'Apple',
// // // // // 		'Some description....',
// // // // // 		100000,
// // // // // 		500,
// // // // // 		true,
// // // // // 		'https://url....',
// // // // // 	],
// // // // // 	[
// // // // // 		'iPhone 15',
// // // // // 		'Apple',
// // // // // 		'Some description....',
// // // // // 		100000,
// // // // // 		500,
// // // // // 		true,
// // // // // 		'https://url....',
// // // // // 	],
// // // // // ];

// // // // const product = [
// // // // 	'iPhone 15',
// // // // 	'Apple',
// // // // 	'Some description....',
// // // // 	100000,
// // // // 	500,
// // // // 	true,
// // // // 	'https://url....',
// // // // ];

// // // // const product2 = {
// // // // 	10: 'hello',
// // // // 	'hello world': 1000,
// // // // 	price: 100000,
// // // // 	name: 'iPhone 15',
// // // // 	brand: 'Apple',
// // // // 	description: 'Some description....',
// // // // 	inStock: 500,
// // // // 	discounted: true,
// // // // 	imageUrl: 'https://url....',
// // // // };

// // // // console.log(product2['price']);
// // // // console.log(product2.price);
// // // // console.log(product2['10']);
// // // // console.log(product2['hello world']);

// // // // // const nums = [1, 2, 3];
// // // // // const nums = {0: 1, 1: 2, 2: 3}

// // // const pallette = {
// // // 	red: '#eb4d4b',
// // // 	yellow: '#f9ca24',
// // // 	blue: '#30336b',
// // // };

// // // // pallette.hello = 'world';
// // // // pallette.red = 'RED';
// // // // delete pallette.yellow;

// // // const userInputValue = 'blue';

// // // console.log(pallette[userInputValue]);

// // // Stack   - []
// // // Queue

// // // for (let count = 0; count <= 10; count++) {
// // // 	console.log(count, 'Hello World');
// // // }

// // for (let count = 10; count >= 0; count++) {
// // 	console.log(count, 'Hello World');
// // }

// // const nums = [12, 34, 56, 34, 78, 54, 23, 12];

// // for (let i = 0; i < nums.length; i++) {
// // 	console.log(nums[i]);
// // }

// const movies = [
// 	{ movieName: 'Inception', rating: 3.8 },
// 	{ movieName: 'Avengers', rating: 2.4 },
// 	{ movieName: 'Iron Man', rating: 2.9 },
// ];

// for (let i = 0; i < movies.length; i++) {
// 	const movie = movies[i];
// 	console.log(`${movie.movieName} has a rating of ${movie.rating}`);
// }

// const word = 'Hello World';
// let reversedWord = '';

// for (let i = word.length - 1; i >= 0; i--) {
// 	// console.log(word[i]);
// 	reversedWord += word[i];
// }

// console.log(reversedWord);

// for (let i = 0; i < 5; i++) {
// 	console.log('OUTER LOOP: ', i);

// 	for (let j = 0; j < 5; j++) {
// 		console.log('    INNER LOOP: ', j);

// 		for (let k = 0; k < 5; k++) {
// 			console.log('        INNER INNER LOOP: ', k);
// 		}
// 	}
// }

// const gameBoard = [
// 	[4, 64, 8, 4],
// 	[128, 32, 4, 16],
// 	[16, 4, 4, 32],
// 	[2, 16, 16, 2],
// ];

// let total = 0;

// for (let i = 0; i < gameBoard.length; i++) {
// 	for (let j = 0; j < gameBoard[i].length; j++) {
// 		// console.log(gameBoard[i][j]);
// 		total += gameBoard[i][j];
// 	}
// }
// console.log(total);

// for (let i = 0; i < 10; i++) {
// 	console.log(i);
// }

// let i = 0;

// while (i < 10) {
// 	console.log(i);
// 	i++;
// }

// const target = Math.floor(Math.random() * 10) + 1;
// let guess = Math.floor(Math.random() * 10) + 1;

// while (target !== guess) {
// 	if (guess === 1) {
// 		break;
// 	}
// 	console.log(`TARGET: ${target} | GUESS: ${guess}`);
// 	guess = Math.floor(Math.random() * 10) + 1;
// }

// console.log(`FINAL\nTARGET: ${target} | GUESS: ${guess}`);

const target = Math.floor(Math.random() * 10) + 1;
let guess = Math.floor(Math.random() * 10) + 1;

while (true) {
	if (target === guess) {
		break;
	}
	console.log(`TARGET: ${target} | GUESS: ${guess}`);
	guess = Math.floor(Math.random() * 10) + 1;
}

console.log(`FINAL\nTARGET: ${target} | GUESS: ${guess}`);
