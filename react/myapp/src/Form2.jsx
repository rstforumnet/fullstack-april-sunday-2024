import React from 'react';

const Form2 = () => {
	const movies = [
		{ id: 'mov1', value: 'The Last Samurai' },
		{ id: 'mov2', value: 'Van Helsing' },
		{ id: 'mov3', value: 'Braveheart' },
		{ id: 'mov4', value: 'A Few Good Men' },
		{ id: 'mov5', value: 'Star Wars: Episode III - Revenge of the Sith' },
	];

	// const [error, setError] = React.useState(null);

	const [username, setUsername] = React.useState('');

	const handleSubmit = (event) => {
		event.preventDefault();
		console.log('Form was submitted');
	};

	// const handleInputChange = (e) => {
	// 	if (e.target.value !== e.target.value.toLowerCase()) {
	// 		setError('Saale sab lowercase rakhna hai');
	// 	} else {
	// 		setError(null);
	// 	}
	// };

	return (
		<div>
			<h3>My Movies</h3>

			{movies.map((movie, i) => (
				<p key={i}>{movie.value}</p>
			))}

			<h3>My Form</h3>

			<form onSubmit={handleSubmit}>
				<div>
					<label htmlFor='username'>Username:</label>
					<input
						id='username'
						type='text'
						value={username}
						onChange={(e) => {
							setUsername(e.target.value.toLowerCase());
						}}
					/>
					<br />
					{/* <span style={{ color: 'red', fontWeight: 'bold' }}>{error}</span> */}
				</div>
				<div>
					<label htmlFor='password'>Password:</label>
					<input id='password' type='text' />
				</div>
				<input type='submit' value='Submit Form' />
			</form>
		</div>
	);
};

export default Form2;
