const Header = ({ children }) => {
	return (
		<header>
			<h1>{children}</h1>
			<nav>
				<a href='#'>Resources</a>
				<a href='#'>Products</a>
				<a href='#'>Services</a>
				<a href='#'>Login</a>
				<a href='#'>Get Started</a>
			</nav>
		</header>
	);
};

export default Header;
