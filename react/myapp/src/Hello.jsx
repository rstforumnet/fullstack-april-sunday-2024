export const Hello = ({ firstName, lastName }) => {
	return (
		<div>
			<h2>
				Hello World from my own component! I am {firstName} {lastName}.
			</h2>
		</div>
	);
};

export const Hello2 = () => {
	return (
		<section>
			<h2>This is something else</h2>
			<p>
				Lorem ipsum dolor sit amet consectetur, adipisicing elit. Harum unde,
				labore voluptates ipsum reprehenderit cum aperiam dolorem rerum
				mollitia. Sit, deleniti tempora numquam reprehenderit hic labore
				adipisci explicabo nisi minus.
			</p>
		</section>
	);
};

export default Hello2;
