import axios from 'axios';
import { useEffect, useState } from 'react';

const Pokemon = () => {
	const [pokemon, setPokemon] = useState([]);
	const [count, setCount] = useState(0);

	useEffect(() => {
		const fetchPokemon = async () => {
			const response = await axios.get('https://pokeapi.co/api/v2/pokemon');
			setPokemon(response.data.results);
			console.log(response.data.results);
		};

		fetchPokemon();
	}, [count]);

	return (
		<div>
			<h1>Pokemon - {count}</h1>
			<button onClick={() => setCount(count + 1)}>Increment</button>
			<ul>
				{pokemon.map((poke) => (
					<li key={poke.name}>{poke.name}</li>
				))}
			</ul>
		</div>
	);
};

export default Pokemon;
