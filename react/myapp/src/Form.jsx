import { useRef } from 'react';

const Form = () => {
	const usernameRef = useRef();
	const passwordRef = useRef();

	const handleSubmit = (e) => {
		// console.log('Form was submitted');
		e.preventDefault();
		// console.dir(e.target[0].value);
		// console.dir(e.target[1].value);
		// console.dir(e.target.elements.username.value);
		// console.dir(e.target.elements.password.value);

		// const { username, password } = e.target.elements;
		// console.dir(username.value);
		// console.dir(password.value);

		console.log(usernameRef.current.value);
		console.log(passwordRef.current.value);

		passwordRef.current.type = 'checkbox';
	};

	return (
		<div>
			<h2>My Form</h2>
			<form onSubmit={handleSubmit}>
				<div>
					<label htmlFor='username'>Username:</label>
					<input id='username' type='text' ref={usernameRef} />
				</div>
				<div>
					<label htmlFor='password'>Password:</label>
					<input id='password' type='password' ref={passwordRef} />
				</div>
				<input type='submit' value='Submit Form' />
			</form>
		</div>
	);
};

export default Form;
