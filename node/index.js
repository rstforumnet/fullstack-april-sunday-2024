// import http from 'http';
const http = require('http');
const fs = require('fs');

const server = http.createServer((req, res) => {
	// console.log(req.url);
	// console.log(req.method);
	// console.log(req.headers);
	// console.log(req);
	// console.log('A request was made');
	// process.exit();

	if (req.url === '/') {
		res.setHeader('Content-Type', 'text/html');
		res.write(`
		<html>
			<head>
				<title>My Application</title>
			</head>
			<body>
				<h1>Hello World</h1>
				<p>This is some sample content.</p>
			</body>
		</html>
	`);
		res.end();
	} else if (req.url === '/about') {
		res.setHeader('Content-Type', 'text/html');
		res.write(`
		<html>
			<head>
				<title>About - My Application</title>
			</head>
			<body>
				<h1>About</h1>
				<form action="/message" method="POST">
          <input type="text" name="message" />
          <button type="submit">Send</button>
        </form>
			</body>
		</html>
	`);
		res.end();
	} else if (req.url === '/message' && req.method === 'POST') {
		const body = [];

		req.on('data', (chunk) => {
			console.log(chunk);
			body.push(chunk);
		});
		req.on('end', () => {
			const parsedBody = Buffer.concat(body).toString();
			const content = parsedBody.split('=')[1];
			fs.writeFileSync('message.txt', content);
		});
		res.statusCode = 302;
		res.setHeader('Location', '/about');
		res.end();
	} else {
		res.setHeader('Content-Type', 'text/html');
		res.write(`
		<html>
			<head>
				<title>Page not found</title>
			</head>
			<body>
				<h1>Page not found</h1>
				<p>This is some sample content.</p>
			</body>
		</html>
	`);
		res.end();
	}
});

server.listen(5000);
